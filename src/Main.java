import Kommander.MasterKommander;

public class Main {
    public static void main (String args[]) {
        MasterKommander masterKommander = new MasterKommander();
        if ( args.length < 2 ) {
            System.out.println("Usage: java Main <keywordsFile> <file>");
            return;
        }

        masterKommander.process(args[1], args[0]);
    }
}
