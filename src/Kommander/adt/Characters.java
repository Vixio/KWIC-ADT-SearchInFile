package Kommander.adt;

public interface Characters {

    void addChar(char chars);

    char charAt(int index);

    int len();
}
