package Kommander.adt;

import java.util.ArrayList;
import java.util.Arrays;

public interface AlphabeticShifts {
    class Pair {
        String keyword;
        ArrayList<Integer> pageIndex;

        public Pair(String keyword, ArrayList<Integer> pageIndex) {
            this.keyword = keyword;
            this.pageIndex = pageIndex;
        }

        public String getKeyword() {
            return keyword;
        }

        public ArrayList<Integer> getPageIndex() {
            return pageIndex;
        }

        @Override
        public String toString() {
            return keyword + ": " + Arrays.toString(pageIndex.toArray());
        }
    }

    void alph();

    Pair iTh(int line);

    int count();

}
