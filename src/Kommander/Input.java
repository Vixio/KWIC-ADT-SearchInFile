package Kommander;

import Kommander.impl.CharactersADT;
import Kommander.impl.KeywordsADT;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class Input {

    public void run(String contentFileName, String keywordFileName) {
        CharactersADT charactersADT = CharactersADT.getInstance();
        KeywordsADT keywordsADT = KeywordsADT.getInstance();

        try {
            byte[] content = Files.readAllBytes(Paths.get(contentFileName));

            for(int i = 0; i < content.length; i++ ) {
                charactersADT.addChar((char) content[i]);
            }

            Scanner scanner = new Scanner(new File(keywordFileName));
            while(scanner.hasNext()) {
                String line = scanner.nextLine().trim();

                if ( ! line.isEmpty() ) {
                    keywordsADT.addWord(line.toLowerCase());
                }
            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
