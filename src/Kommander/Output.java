package Kommander;

import Kommander.impl.AlphabethicShiftADT;

public class Output {
    public void run() {
        AlphabethicShiftADT alphabeticShift = AlphabethicShiftADT.getInstance();
        for(int i = 0; i < alphabeticShift.count(); i++) {
            System.out.println(alphabeticShift.iTh(i));
        }
    }
}
