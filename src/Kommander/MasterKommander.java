package Kommander;

import Kommander.adt.Characters;
import Kommander.impl.AlphabethicShiftADT;
import Kommander.impl.WordIndicesADT;

public class MasterKommander {
    private Characters characters;
    private Input input;
    private Output output;


    public MasterKommander() {
        input = new Input();
        output = new Output();
    }

    public void process(String contentFileName, String keywordFileName){
        input.run(contentFileName, keywordFileName);
        WordIndicesADT.getInstance().setup();
        AlphabethicShiftADT.getInstance().alph();
        output.run();
    }
}
