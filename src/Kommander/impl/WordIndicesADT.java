package Kommander.impl;


import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Hashtable;

public class WordIndicesADT {
    Hashtable<String, ArrayList<Integer> > glos;
    private static WordIndicesADT sInstance;

    private WordIndicesADT() {
        glos = new Hashtable<>();
    }

    public void setup() {
        KeywordsADT keywordsADT = KeywordsADT.getInstance();
        CharactersADT charactersADT = CharactersADT.getInstance();

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            StringBuilder sb = new StringBuilder();
            for ( int i = 0; i < charactersADT.len(); i++) {
                sb.append(charactersADT.charAt(i));
            }

            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
            Document document = builder.parse(byteArrayInputStream);
            NodeList list = document.getElementsByTagName("page");
            for ( int i = 0; i < list.getLength(); i++ ) {
                String text = list.item(i).getTextContent().toLowerCase();

                for(int j = 0; j < keywordsADT.len(); j++) {
                    String currentKeyword = keywordsADT.wordAt(j);
                    int foundAtIndex = text.indexOf(currentKeyword);

                    if ( foundAtIndex != -1 ) {
                        if ( ! glos.containsKey( currentKeyword ) ) {
                            glos.put(currentKeyword, new ArrayList());
                        }

                        glos.get(currentKeyword).add(i + 1);
                    }
                }
            }

        } catch (Exception e) {

        }
    }

    public ArrayList<Integer> getIndices(String word) {
        return glos.get(word);
    }

    public static WordIndicesADT getInstance() {
        if ( sInstance == null ) {
            sInstance = new WordIndicesADT();
        }

        return sInstance;
    }
}
