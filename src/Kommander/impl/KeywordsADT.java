package Kommander.impl;

import java.util.ArrayList;

public class KeywordsADT {
    private static KeywordsADT sInstance;
    private ArrayList<String> words;

    private KeywordsADT() {
        words = new ArrayList<>();
    }

    public void addWord(String word) {
        words.add(word.toLowerCase());
    }

    public String wordAt(int index) {
        return words.get(index);
    }

    public int len() {
        return words.size();
    }

    public static KeywordsADT getInstance() {
        if ( sInstance == null ) {
            sInstance = new KeywordsADT();
        }

        return sInstance;
    }
}
