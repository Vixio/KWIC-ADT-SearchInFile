package Kommander.impl;

import Kommander.adt.Characters;

import java.util.ArrayList;;

public class CharactersADT implements Characters {

    private static CharactersADT sInstance;
    private ArrayList<Character> characterses;

    private CharactersADT() {
        characterses = new ArrayList<>();
    }

    @Override
    public void addChar(char character) {
        characterses.add(character);
    }

    @Override
    public char charAt(int index) {
        return characterses.get(index);
    }

    @Override
    public int len() {
        return characterses.size();
    }

    public static CharactersADT getInstance() {
        if ( sInstance == null ) {
            sInstance = new CharactersADT();
        }

        return sInstance;
    }
}
