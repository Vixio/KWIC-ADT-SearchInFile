package Kommander.impl;

import Kommander.adt.AlphabeticShifts;

import java.util.ArrayList;
import java.util.Collections;

public class AlphabethicShiftADT implements AlphabeticShifts {
    private static AlphabethicShiftADT sInstance;
    private ArrayList<Pair> lines;

    private AlphabethicShiftADT() {
        this.lines = new ArrayList<>();
    }

    @Override
    public void alph() {
        WordIndicesADT wordIndices = WordIndicesADT.getInstance();
        KeywordsADT keywordsADT = KeywordsADT.getInstance();

        for(int i = 0; i < keywordsADT.len(); i++) {
            String keyword = keywordsADT.wordAt(i);

            ArrayList<Integer> pageIndices = wordIndices.getIndices(keyword);
            if ( pageIndices != null ) {
                lines.add(new Pair(keyword, pageIndices));
            }
        }

        Collections.sort(lines, (Pair p1, Pair p2) -> {
            return p1.getKeyword().compareTo(p2.getKeyword());
        });
    }

    @Override
    public Pair iTh(int line) {
        return lines.get(line);
    }

    @Override
    public int count() {
        return lines.size();
    }

    public static AlphabethicShiftADT getInstance() {
        if ( sInstance == null ) {
            sInstance = new AlphabethicShiftADT();
        }

        return  sInstance;
    }
}
